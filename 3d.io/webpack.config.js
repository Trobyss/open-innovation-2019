const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    mode: "production",
    entry: "./dev/main.js", // string | object | array
    output: {
        path: path.resolve(__dirname, "build"), // string
        filename: "bundle.js", // string
    },
    watch: true,
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: [
                    path.resolve(__dirname, "app")
                ],
                exclude: [
                    path.resolve(__dirname, "app/demo-files"),
                ],
                use: {
                    loader: 'babel-loader',
                    query: {
                        presets: ["es2015"]
                    }
                }
            },
        ]
    },
    // plugins: [
    //     new UglifyJSPlugin()
    // ]
}
