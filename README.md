## Installations

 - Docker :

```bash

cd wordpress-docker

docker-compose up

```

 - React App

```bash

cd react-inno

npm install

npm run

```

## Utilisation :

 - Docker & React app

```bash

cd wordpress-docker && docker-compose up

cd react-inno && npm run start

```
## Informations :

 - ip :
	 - Wordpress : http://192.168.99.100:8080
	 - PhpMyAdmin : http://192.168.99.100:8181
	 - ReactApp: http://localhost:3000
	
 - Username and Password :
	 - mysql_database : wordpress
	 - mysql_user : wp_user
	 - mysql_password : root
	 - wordpress_username : Innovation
	 - wordpress_password: Innovation2019
